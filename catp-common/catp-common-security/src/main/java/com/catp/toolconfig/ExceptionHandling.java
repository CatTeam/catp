package com.catp.toolconfig;

import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.security.web.util.ThrowableAnalyzer;

/**
 * @Author LaoCat
 * @Date 2019/7/19 11:45
 * @Version 1.0.0
 **/
public class ExceptionHandling implements WebResponseExceptionTranslator {
    private ThrowableAnalyzer throwableAnalyzer = new ThrowableAnalyzer();

    @Override
    public ResponseEntity translate(Exception e) throws Exception {
        return null;
    }
}
