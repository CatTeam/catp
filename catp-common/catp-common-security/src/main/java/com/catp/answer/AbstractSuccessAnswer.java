package com.catp.answer;

import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * @Author LaoCat
 * @Date 2019/7/22 10:21
 * @Version 1.0.0
 **/
public abstract class AbstractSuccessAnswer implements ApplicationListener<AuthenticationSuccessEvent> {

    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent event) {
        Authentication authentication = (Authentication) event.getSource();

        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        if (null != authorities) {
            exec(authentication);
        }
    }

    // 登录成功的处理方法
    public abstract void exec(Authentication authentication);
}
