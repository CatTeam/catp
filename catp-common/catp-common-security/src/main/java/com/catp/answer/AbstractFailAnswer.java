package com.catp.answer;

import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

/**
 * 失败处理器
 *
 * @Author LaoCat
 * @Date 2019/7/22 10:21
 * @Version 1.0.0
 **/
public abstract class AbstractFailAnswer implements ApplicationListener<AbstractAuthenticationFailureEvent> {

    @Override
    public void onApplicationEvent(AbstractAuthenticationFailureEvent event) {
        Authentication authentication = (Authentication) event.getSource();
        AuthenticationException exception = event.getException();
        exec(authentication, exception);
    }

    // 处理登录失败
    public abstract void exec(Authentication authentication, AuthenticationException exception);
}
