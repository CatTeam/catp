package com.catp.source;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @Author LaoCat
 * @Date 2019/7/19 11:04
 * @Version 1.0.0
 **/
public interface UserSourceService extends UserDetailsService {

}
