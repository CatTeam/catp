package com.catp.source;

import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;

import javax.sql.DataSource;

/**
 * @Author LaoCat
 * @Date 2019/7/19 11:27
 * @Version 1.0.0
 **/
public class ClientsSourceService extends JdbcClientDetailsService {

    public ClientsSourceService(DataSource dataSource) {
        super(dataSource);
    }
}
