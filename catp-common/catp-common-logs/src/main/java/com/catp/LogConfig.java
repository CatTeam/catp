package com.catp;

import com.catp.admin.api.feign.LogService;
import com.catp.aspect.LogAspect;
import com.catp.listener.LogListener;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @Author LaoCat
 * @Date 2019/7/18 16:49
 * @Version 1.0.0
 **/
@EnableAsync
@Configuration
@ConditionalOnWebApplication
@AllArgsConstructor
public class LogConfig {
    private final LogService logService;

    @Bean
    public LogListener logListener() {
        return new LogListener(logService);
    }

    @Bean
    public LogAspect logAspect(ApplicationEventPublisher publisher) {
        return new LogAspect(publisher);
    }
}
