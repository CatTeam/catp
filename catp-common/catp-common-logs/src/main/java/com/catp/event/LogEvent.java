package com.catp.event;

import com.catp.admin.api.entity.ProLog;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author LaoCat
 * @Date 2019/7/18 16:50
 * @Version 1.0.0
 **/
@AllArgsConstructor
public class LogEvent {
    @Getter
    private final ProLog proLog;
}
