package com.catp.aspect;

import com.catp.admin.api.entity.ProLog;
import com.catp.annotation.Log;
import com.catp.event.LogEvent;
import com.catp.utils.LogUtils;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.ApplicationEventPublisher;

/**
 * @Author LaoCat
 * @Date 2019/7/18 16:49
 * @Version 1.0.0
 **/
@Aspect
@AllArgsConstructor
public class LogAspect {
    private final ApplicationEventPublisher publisher;

    @SneakyThrows
    @Around("@annotation(log)")
    public Object around(ProceedingJoinPoint point, Log log) {
        String className = point.getTarget().getClass().getName();

        ProLog proLog = LogUtils.getProLog();
        proLog.setLogName(log.value());
        proLog.setClassName(className);

        long af = System.currentTimeMillis();
        Object proceed = point.proceed();
        long bf = System.currentTimeMillis();
        proLog.setExecTime(af - bf);
        publisher.publishEvent(new LogEvent(proLog));
        return proceed;
    }

}
