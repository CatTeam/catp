package com.catp.utils;

import cn.hutool.core.util.URLUtil;
import cn.hutool.extra.servlet.ServletUtil;
import cn.hutool.http.HttpUtil;
import com.catp.admin.api.entity.ProLog;
import lombok.experimental.UtilityClass;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * @Author LaoCat
 * @Date 2019/7/18 16:49
 * @Version 1.0.0
 **/
@UtilityClass
public class LogUtils {
    public ProLog getProLog() {
        HttpServletRequest request = ((ServletRequestAttributes) Objects
                .requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        ProLog log = new ProLog();
        log.setCreator(Objects.requireNonNull(getUserName()));
        log.setIp(ServletUtil.getClientIP(request));
        log.setRequestUrl(URLUtil.getPath(request.getRequestURI()));
        log.setMethod(request.getMethod());
        log.setParams(HttpUtil.toParams(request.getParameterMap()));
        log.setServiceId(getServiceId());
        return log;
    }

    /**
     * 添加授权后从authentication 中获取 clientid
     *
     * @author : blackCat
     * @date : 2019/7/19 8:52
     */
    private String getServiceId() {
        return null;
    }


    /**
     * 添加授权后从authentication 中获取
     *
     * @author : blackCat
     * @date : 2019/7/19 8:47
     */
    private String getUserName() {
        return null;
    }


}
