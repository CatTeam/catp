package com.catp.listener;

import com.catp.admin.api.entity.ProLog;
import com.catp.admin.api.feign.LogService;
import com.catp.event.LogEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;

/**
 * @Author LaoCat
 * @Date 2019/7/18 16:51
 * @Version 1.0.0
 **/
@Slf4j
@AllArgsConstructor
public class LogListener {
    private final LogService logService;

    @Async
    @Order
    @EventListener(LogEvent.class)
    public void insertLog(LogEvent logEvent) {
        ProLog proLog = logEvent.getProLog();
        logService.insertLog(proLog);
    }
}
