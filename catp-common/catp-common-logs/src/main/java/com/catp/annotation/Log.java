package com.catp.annotation;

import java.lang.annotation.*;

/**
 * @Author LaoCat
 * @Date 2019/7/18 16:47
 * @Version 1.0.0
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {

    String value() default "catp";
}
