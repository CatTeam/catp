package com.catp.constant;

/**
 * @author LaoCat
 * @version 1.0.0
 * @date 2019/7/31 9:46
 **/
public interface AuthorizeConstant {

    /**
     * 自定义key 过滤
     */
    String CATP_FLAG = "catp:";
    /**
     * oauth Prefix
     */
    String OAUTH_PREFIX = "catp:oauth:";
}
