package com.catp.mybatisplus;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @Author LaoCat
 * @Date 2019/7/11 14:42
 * @Version 1.0.0
 **/
@Configuration
@EnableTransactionManagement
@MapperScan("com.catp.*.mapper")
public class MybatisPlusConfig {

    /**
     * 分页插件
     *
     * @author : blackCat
     * @date : 2019/7/11 14:45
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

}
