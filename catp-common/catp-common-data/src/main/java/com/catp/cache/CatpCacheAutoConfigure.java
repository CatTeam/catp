package com.catp.cache;

import cn.hutool.core.collection.CollUtil;
import com.catp.constant.AuthorizeConstant;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;

import java.time.Duration;
import java.util.Map;

/**
 * @author LaoCat
 * @version 1.0.0
 * @date 2019/7/31 9:41
 **/
@Configuration
@EnableCaching
public class CatpCacheAutoConfigure {

    @Bean
    public KeyGenerator simpleKeyGenerator() {
        return (o, method, objects) -> {
            StringBuilder strBuilder = new StringBuilder();
            strBuilder.append(AuthorizeConstant.CATP_FLAG);
            strBuilder.append(":");
            strBuilder.append(method.getName());
            strBuilder.append("[");
            for (Object object : objects) {
                strBuilder.append(object.toString());
            }
            strBuilder.append("]");
            return strBuilder.toString();
        };
    }

    @Bean
    public CacheManager cacheManager(RedisConnectionFactory redisConnectionFactory) {
        return new RedisCacheManager(
                RedisCacheWriter.nonLockingRedisCacheWriter(redisConnectionFactory),
                this.getRedisCacheWithTtl(600L),
                this.getRedisCacheWithMap()
        );
    }

    private Map<String, RedisCacheConfiguration> getRedisCacheWithMap() {
        Map<String, RedisCacheConfiguration> redisCacheConfigurationMap = CollUtil.newHashMap();
        return redisCacheConfigurationMap;
    }

    private RedisCacheConfiguration getRedisCacheWithTtl(long second) {
        JdkSerializationRedisSerializer jdkSerializationRedisSerializer = new JdkSerializationRedisSerializer();

        RedisCacheConfiguration redisCacheConfiguration = RedisCacheConfiguration.defaultCacheConfig();
        redisCacheConfiguration = redisCacheConfiguration.serializeValuesWith(RedisSerializationContext
                .SerializationPair
                .fromSerializer(jdkSerializationRedisSerializer)).entryTtl(Duration.ofSeconds(second));
        return redisCacheConfiguration;
    }
}
