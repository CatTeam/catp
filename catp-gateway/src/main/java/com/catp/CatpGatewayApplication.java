package com.catp;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * @Author LaoCat
 * @Date 2019/7/10 14:58
 * @Version 1.0.0
 **/
@SpringCloudApplication
public class CatpGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(CatpGatewayApplication.class, args);
    }
}
