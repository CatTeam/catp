package com.catp.config;

import com.catp.constant.AuthorizeConstant;
import com.catp.source.ClientsSourceService;
import com.catp.source.UserSourceService;
import com.catp.toolconfig.ExceptionHandling;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;

import javax.sql.DataSource;

/**
 * @Author LaoCat
 * @Date 2019/7/19 10:29
 * @Version 1.0.0
 **/
@Configuration
@EnableAuthorizationServer
@AllArgsConstructor
public class AuthorizationConfig extends AuthorizationServerConfigurerAdapter {
    private final DataSource dataSource;
    private final RedisConnectionFactory redisConnectionFactory;

    private final AuthenticationManager authorizationManager;
    private final UserSourceService userSourceService;

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security
                .checkTokenAccess("isAuthenticated()")
                .allowFormAuthenticationForClients();
    }

    @Override
    @SneakyThrows
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        ClientsSourceService clientsSource = new ClientsSourceService(dataSource);
        clientsSource.setFindClientDetailsSql("");
        clientsSource.setSelectClientDetailsSql("");
        clients.withClientDetails(clientsSource);
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints
                .allowedTokenEndpointRequestMethods(HttpMethod.GET, HttpMethod.POST)
                .tokenStore(customizeTokenStore())
                .tokenEnhancer(customizeTokenEnhancer())
                .userDetailsService(userSourceService)
                .authenticationManager(authorizationManager)
                .reuseRefreshTokens(false)
                .pathMapping("/oauth/confirm_access", "/customize_oauth/confirm_access")
                .exceptionTranslator(new ExceptionHandling());
    }

    /**
     * 给token设置一个前缀 并选择redis存放
     *
     * @author : blackCat
     * @date : 2019/7/22 9:59
     */
    private TokenStore customizeTokenStore() {
        RedisTokenStore redisTokenStore = new RedisTokenStore(redisConnectionFactory);
        redisTokenStore.setPrefix(AuthorizeConstant.OAUTH_PREFIX);
        return redisTokenStore;
    }

    private TokenEnhancer customizeTokenEnhancer() {

        return null;
    }
}
