package com.catp.answer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

/**
 * 失败处理
 *
 * @Author LaoCat
 * @Date 2019/7/22 10:15
 * @Version 1.0.0
 **/
@Component
@Slf4j
public class LoginFailM extends AbstractFailAnswer {

    @Override
    public void exec(Authentication authentication, AuthenticationException exception) {
        exception.printStackTrace();
        log.error("登录异常 --> 用户为{},异常信息为{}", authentication.getPrincipal(), exception.getLocalizedMessage());
    }
}
