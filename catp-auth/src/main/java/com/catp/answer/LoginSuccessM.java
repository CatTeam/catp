package com.catp.answer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

/**
 * 成功处理
 *
 * @Author LaoCat
 * @Date 2019/7/22 10:15
 * @Version 1.0.0
 **/
@Component
@Slf4j
public class LoginSuccessM extends AbstractSuccessAnswer {

    @Override
    public void exec(Authentication authentication) {
        log.info("登录成功 --> 用户为{}", authentication.getPrincipal());
    }
}
