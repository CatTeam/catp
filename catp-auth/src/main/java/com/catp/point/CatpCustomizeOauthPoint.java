package com.catp.point;

import cn.hutool.core.util.StrUtil;
import com.catp.constant.AuthorizeConstant;
import lombok.AllArgsConstructor;
import org.springframework.cache.CacheManager;
import org.springframework.http.HttpHeaders;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @Author LaoCat
 * @Date 2019/7/22 10:36
 * @Version 1.0.0
 **/
@RestController
@AllArgsConstructor
@RequestMapping("/customize_oauth")
public class CatpCustomizeOauthPoint {

    private final TokenStore tokenStore;
    private final CacheManager cacheManager;
    private final ClientDetailsService clientDetailsService;

    /**
     * 确认
     *
     * @author : blackCat
     * @date : 2019/7/22 10:53
     */
    @GetMapping("/confirm_access")
    public ModelAndView confirmAccess(HttpServletRequest request, HttpSession session, ModelAndView modelAndView) {
        Map<String, Object> scopeList = (Map<String, Object>) request.getAttribute("scopes");
        modelAndView.addObject("scopeList", scopeList.keySet());

        Object auth = session.getAttribute("authorizationRequest");
        if (auth != null) {
            AuthorizationRequest authorizationRequest = (AuthorizationRequest) auth;
            ClientDetails clientDetails = clientDetailsService.loadClientByClientId(authorizationRequest.getClientId());
            modelAndView.addObject("app", clientDetails.getAdditionalInformation());
            modelAndView.addObject("user", "12345654");
        }

        modelAndView.setViewName("ftl/confirm");
        return modelAndView;
    }

    @DeleteMapping("/logout")
    public Boolean logout(@RequestHeader(value = HttpHeaders.AUTHORIZATION, required = false) String authHeader) {
        if (StrUtil.isBlank(authHeader)) {
            return Boolean.FALSE;
        }

        String tokenValue = authHeader.replace(OAuth2AccessToken.BEARER_TYPE, StrUtil.EMPTY).trim();
        OAuth2AccessToken accessToken = tokenStore.readAccessToken(tokenValue);
        if (accessToken == null || StrUtil.isBlank(accessToken.getValue())) {
            return Boolean.FALSE;
        }

        OAuth2Authentication auth2Authentication = tokenStore.readAuthentication(accessToken);
        cacheManager.getCache(AuthorizeConstant.OAUTH_PREFIX + auth2Authentication).clear();
        tokenStore.removeAccessToken(accessToken);
        return Boolean.TRUE;
    }

    /**
     * 认页面证
     *
     * @author : blackCat
     * @date : 2019/7/22 10:43
     */
    @GetMapping("login")
    public ModelAndView certification(ModelAndView modelAndView) {
        modelAndView.setViewName("");
        return modelAndView;
    }
}
