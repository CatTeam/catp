package com.catp;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * @Author LaoCat
 * @Date 2019/7/10 14:59
 * @Version 1.0.0
 **/
@SpringCloudApplication
public class CatpAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(CatpAuthApplication.class, args);
    }
}
