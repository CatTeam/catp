package com.catp.admin.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author LaoCat
 * @Date 2019/7/19 8:23
 * @Version 1.0.0
 **/
@Data
public class ProLog implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 日志id
     */
    @TableId(type = IdType.ID_WORKER_STR)
    private String logId;
    /**
     * 日志名称
     */
    private String logName;
    /**
     * 创建类型
     */
    private String type;
    /**
     * 创建者
     */
    private String creator;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 请求ip地址
     */
    private String ip;
    /**
     * 请求url
     */
    private String requestUrl;
    /**
     * 请求的方式
     */
    private String method;
    /**
     * 请求参数
     */
    private String params;
    /**
     * 异常信息
     */
    private String exceptionInfo;
    /**
     * 服务id
     */
    private String serviceId;
    /**
     * 类名
     */
    private String className;
    /**
     * 执行时间
     */
    private Long execTime;

}
