package com.catp.admin.api.feign;

import com.catp.admin.api.entity.ProLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @Author LaoCat
 * @Date 2019/7/18 16:55
 * @Version 1.0.0
 **/
@FeignClient(contextId = "logService", value = "catp-property-service")
public interface LogService {

    @PostMapping("/log/")
    Boolean insertLog(@RequestBody ProLog proLog);
}
