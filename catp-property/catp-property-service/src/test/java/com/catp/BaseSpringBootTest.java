package com.catp;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Author LaoCat
 * @Date 2019/7/12 10:29
 * @Version 1.0.0
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class BaseSpringBootTest {
}
