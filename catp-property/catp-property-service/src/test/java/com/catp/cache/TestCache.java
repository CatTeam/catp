package com.catp.cache;

import com.catp.BaseSpringBootTest;
import com.catp.property.entity.TestUser;
import com.catp.property.service.TestUserService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author LaoCat
 * @Date 2019/7/12 10:31
 * @Version 1.0.0
 **/
public class TestCache extends BaseSpringBootTest {

    @Autowired
    private TestUserService testUserService;

    @Test
    public void testCache() {
        TestUser testUser = testUserService.testUserById("12312");
        System.out.println(testUser);

    }

}
