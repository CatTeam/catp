package com.catp;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * @Author LaoCat
 * @Date 2019/7/10 14:57
 * @Version 1.0.0
 **/
@SpringCloudApplication
public class CatpPropertyApplication {

    public static void main(String[] args) {
        SpringApplication.run(CatpPropertyApplication.class, args);
    }
}
