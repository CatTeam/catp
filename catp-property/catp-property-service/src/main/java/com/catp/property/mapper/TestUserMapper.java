package com.catp.property.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.catp.property.entity.TestUser;

/**
 * @Author LaoCat
 * @Date 2019/7/11 15:17
 * @Version 1.0.0
 **/
public interface TestUserMapper extends BaseMapper<TestUser> {

}
