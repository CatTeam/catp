package com.catp.property.controller;

import com.catp.property.entity.TestUser;
import com.catp.property.service.TestUserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author LaoCat
 * @Date 2019/7/11 15:14
 * @Version 1.0.0
 **/
@RestController
@AllArgsConstructor
@RequestMapping("/test")
public class TestController {

    private final TestUserService testUserService;

    @GetMapping("/{id}")
    public TestUser testUserById(@PathVariable("id") String id) {
        TestUser testUser = testUserService.getById(id);
        return testUser;
    }
}
