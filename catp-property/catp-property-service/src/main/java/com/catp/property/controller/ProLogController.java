package com.catp.property.controller;

import com.catp.admin.api.entity.ProLog;
import com.catp.property.service.ProLogService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author LaoCat
 * @Date 2019/7/19 9:21
 * @Version 1.0.0
 **/
@RestController
@AllArgsConstructor
@RequestMapping("/log")
public class ProLogController {
    private final ProLogService proLogService;

    /**
     * 存入日志
     *
     * @author : blackCat
     * @date : 2019/7/19 9:29
     */
    @PostMapping
    public Boolean insertLog(@RequestBody ProLog proLog) {
        return proLogService.save(proLog);
    }
}
