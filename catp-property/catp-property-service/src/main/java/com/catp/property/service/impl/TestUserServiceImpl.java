package com.catp.property.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.catp.constant.CacheConstant;
import com.catp.property.entity.TestUser;
import com.catp.property.mapper.TestUserMapper;
import com.catp.property.service.TestUserService;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @Author LaoCat
 * @Date 2019/7/11 15:16
 * @Version 1.0.0
 **/
@Service
@AllArgsConstructor
public class TestUserServiceImpl extends ServiceImpl<TestUserMapper, TestUser> implements TestUserService {
    private final TestUserMapper testUserMapper;

    @Override
    @Cacheable(value = CacheConstant.TEST_USER, keyGenerator = "simpleKeyGenerator")
    public TestUser testUserById(String id) {
        return this.getById(id);
    }
}
