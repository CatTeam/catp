package com.catp.property.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author LaoCat
 * @Date 2019/7/11 15:14
 * @Version 1.0.0
 **/
@Data
@EqualsAndHashCode(callSuper = true)
public class TestUser extends Model<TestUser> {

    @TableId(type = IdType.ID_WORKER_STR)
    private String userId;

    private String userName;

}
