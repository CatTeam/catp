package com.catp.property.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.catp.admin.api.entity.ProLog;
import com.catp.property.mapper.ProLogMapper;
import com.catp.property.service.ProLogService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @Author LaoCat
 * @Date 2019/7/19 9:24
 * @Version 1.0.0
 **/
@Service
@AllArgsConstructor
public class ProLogServiceImpl extends ServiceImpl<ProLogMapper, ProLog> implements ProLogService {
    private final ProLogMapper proLogMapper;


}
