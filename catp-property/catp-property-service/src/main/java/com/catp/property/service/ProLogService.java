package com.catp.property.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.catp.admin.api.entity.ProLog;

/**
 * @Author LaoCat
 * @Date 2019/7/19 9:24
 * @Version 1.0.0
 **/
public interface ProLogService extends IService<ProLog> {
}
