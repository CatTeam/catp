package com.catp.property.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.catp.admin.api.entity.ProLog;

/**
 * @Author LaoCat
 * @Date 2019/7/19 9:25
 * @Version 1.0.0
 **/
public interface ProLogMapper extends BaseMapper<ProLog> {

}
