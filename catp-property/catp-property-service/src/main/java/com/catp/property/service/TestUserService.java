package com.catp.property.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.catp.property.entity.TestUser;

/**
 * @Author LaoCat
 * @Date 2019/7/11 15:16
 * @Version 1.0.0
 **/
public interface TestUserService extends IService<TestUser> {

    /**
     * @param id
     * @return
     */
    TestUser testUserById(String id);
}
