私服地址: [http://114.67.86.152:8081](http://114.67.86.152:8081/)

私服账号:

|  账号  | 密码      |
| :----: | --------- |
|  klp   | klp123456 |
| catone | catone    |



maven setting.xml需要配置:

[servers]

```maven
<server>  
	<id>maven-public</id>  
	<username>账号</username>  
	<password>密码</password>  
</server>
<server>
	<id>java-catone</id>  
	<username>账号</username>  
	<password>密码</password>  
</server>  
```

[mirrors]

```maven
<mirror>
   <id>alimaven</id>
   <name>aliyun maven</name>
   <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
   <mirrorOf>central</mirrorOf>
</mirror>
```

------

```
这俩个jar存放在私仓
<dependency>
    <groupId>com.alibaba.nacos</groupId>
    <artifactId>nacos-config</artifactId>
    <version>1.0.1</version>
</dependency>
<dependency>
    <groupId>com.alibaba.nacos</groupId>
    <artifactId>nacos-naming</artifactId>
    <version>1.0.1</version>
</dependency>
```

